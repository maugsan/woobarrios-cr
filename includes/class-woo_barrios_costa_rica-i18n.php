<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://profiles.wordpress.org/maugsan/
 * @since      1.0.0
 *
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/includes
 * @author     Mauricio Sánchez <maugsan@protonmail.com>
 */
class Woo_barrios_costa_rica_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'woo_barrios_costa_rica',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
