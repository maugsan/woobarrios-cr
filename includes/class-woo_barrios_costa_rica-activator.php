<?php

/**
 * Fired during plugin activation
 *
 * @link       https://profiles.wordpress.org/maugsan/
 * @since      1.0.0
 *
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/includes
 * @author     Mauricio Sánchez <maugsan@protonmail.com>
 */
class Woo_barrios_costa_rica_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
