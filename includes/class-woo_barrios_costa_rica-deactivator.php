<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://profiles.wordpress.org/maugsan/
 * @since      1.0.0
 *
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/includes
 * @author     Mauricio Sánchez <maugsan@protonmail.com>
 */
class Woo_barrios_costa_rica_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
