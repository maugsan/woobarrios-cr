<?php
/**
 * Costa Rica
 *
 * @author  Mauricio Sánchez U. <mauricio@pixelcr.com>
 * @version 1.0.1
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/
 global $wpdb;
 $estados = array();

 for ($i=1; $i <= 7; $i++) {
   $results = $wpdb->get_results( "select PROVINCIA, CANTON, DISTRITO,BARRIO ,COD_DISTRITO from wp_correos where provincia = '".$i."' AND OBSERVACIONES  != '1';", ARRAY_A );
   foreach ($results as $key => $value) {
   $estados[$i][ $key. '-'.$value['COD_DISTRITO'] ] = $value['COD_DISTRITO'] .' - '. $value['CANTON'] .', '. $value['DISTRITO'] .', '. $value['BARRIO'];
   }
 }
 //file_put_contents('places.txt', var_export($estados));
 // $var_str = var_export($estados, true);
 // $var = "<?php\n\n\$text = $var_str;\n\n?>";
 // file_put_contents('places.php', $var);

$places['CR'] = $estados;
