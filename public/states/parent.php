
<?php
/**
 * Guatemalan departments
 *
 * @author  Ronald Montenegro <rodmontgt@gmail.com> http://espaciogt.wordpress.com
 * @version 1.0.0
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
global $states;
global $wpdb;

$estados = array();
$results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}ubicacionProvincia", ARRAY_A );
foreach ($results as $key => $value) {
$estados[ $value['id'] ] =  $value['nombre'] ;

}

//file_put_contents('states.txt', var_export($estados));


// $var_str = var_export($estados, true);
// $var = "<?php\n\n\$text = $var_str;\n\n?>";
// file_put_contents('states.php', $var);

$states['CR'] = $estados;
