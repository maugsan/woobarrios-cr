<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://profiles.wordpress.org/maugsan/
 * @since      1.0.0
 *
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/public/partials
 */
?>
