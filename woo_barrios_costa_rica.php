<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://profiles.wordpress.org/maugsan/
 * @since             1.0.0
 * @package           Woo_barrios_costa_rica
 *
 * @wordpress-plugin
 * Plugin Name:       Woo Barrios Costa Rica
 * Plugin URI:        https://pintocommmerce.com
 * Description:       Este plugin permite agregar la división territorial del Costa Rica a Woocommerce
 * Author:            Mauricio Sánchez
 * Author URI:        https://profiles.wordpress.org/maugsan/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woo_barrios_costa_rica
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOO_BARRIOS_COSTA_RICA_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo_barrios_costa_rica-activator.php
 */
function activate_woo_barrios_costa_rica() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo_barrios_costa_rica-activator.php';
	Woo_barrios_costa_rica_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo_barrios_costa_rica-deactivator.php
 */
function deactivate_woo_barrios_costa_rica() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo_barrios_costa_rica-deactivator.php';
	Woo_barrios_costa_rica_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woo_barrios_costa_rica' );
register_deactivation_hook( __FILE__, 'deactivate_woo_barrios_costa_rica' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo_barrios_costa_rica.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woo_barrios_costa_rica() {

	$plugin = new Woo_barrios_costa_rica();
	$plugin->run();

}

// WooCommerce Checkout Fields Hook
add_filter( 'woocommerce_checkout_fields' , 'custom_wc_checkout_fields' );
// Change order comments placeholder and label, and set billing phone number to not required.
function custom_wc_checkout_fields( $fields ) {
		$fields['order']['order_comments']['placeholder'] = 'Enter your placeholder text here.';
		$fields['order']['order_comments']['label'] = 'Enter your label here.';
		$fields['billing']['billing_city']['required'] = false;
    $fields['billing']['billing_state']['required'] = false;
		$fields['billing']['billing_phone']['required'] = false;

		return $fields;
}


run_woo_barrios_costa_rica();
