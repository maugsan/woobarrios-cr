(function( $ ) {
	'use strict';

	$('.cart_totals h2').html('Total del carrito');

	$("#billing_cedula").attr("placeholder", "Cédula de Identidad o Cédula de Jurídica");
	//deshabilita id="billing_postcode"
	$("#billing_postcode").attr("readonly","true");
	// document.getElementById("billing_postcode").readOnly = true;
	//
	//cuando id="billing_city" cambia, id="billing_postcode" se llena con el valor antes de la separación (' - ') dentro de id="billing_city"
	$("#billing_city").on("change", function(){
		var billing_city = $(this).val().split(' - ');
		// console.log(billing_city[0]);
		$("#billing_postcode").val(billing_city[0]);
	});
	//

	//deshabilita id="shipping_postcode"
	$("#shipping_postcode").attr("readonly","true");
	// document.getElementById("shipping_postcode").readOnly = true;
	//
	//cuando id="shipping_city" cambia, id="shipping_postcode" se llena con el valor antes de la separación (' - ') dentro de id="shipping_city"
	$("#shipping_city").on("change", function(){
		var shipping_city = $(this).val().split(' - ');
		// console.log(shipping_city[0]);
		$("#shipping_postcode").val(shipping_city[0]);
	});
	//



	$(document).ready(function(){


		/*$('#billing_cedula').attr('minlength', 9);*/
		$('#billing_cedula').attr('maxlength', 10);

		$('#billing_cedula').keyup(function () {
			this.value = this.value.replace(/[^0-9]/g,'');
		});

		/*$('#billing_cedula').attr('minlength', 9);*/
		$('#billing_phone').attr('maxlength', 8);

		$('#billing_phone').keyup(function () {
			this.value = this.value.replace(/[^0-9]/g,'');
		});



	});

		})( jQuery );
