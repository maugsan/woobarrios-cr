<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://profiles.wordpress.org/maugsan/
 * @since      1.0.0
 *
 * @package    Woo_barrios_costa_rica
 * @subpackage Woo_barrios_costa_rica/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
